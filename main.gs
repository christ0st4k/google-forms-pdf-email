function onFormSubmit() {
    var form = FormApp.getActiveForm();
    var all_form_responses = form.getResponses();
    var last_response_array = all_form_responses[all_form_responses.length-1].getItemResponses();
    sendMail(last_response_array,createPDF(docCreation(last_response_array)));
}

function docCreation(array){  
    var filename = array[1].getResponse() + '_' + array[0].getResponse();  
    doc = DocumentApp.create(filename);  
    var docFile = DriveApp.getFileById(doc.getId());  
    DriveApp.getFoldersByName('DocumentsFromResponses').next().addFile(docFile); 

    // Variable to keep text fields from the Form
    var person = {
        fname: array[0].getResponse(),
        lname: array[1].getResponse(),
        Fname: array[2].getResponse(),
        Mname: array[3].getResponse(),
        Fjob: array[4].getResponse(),
        Mjob: array[5].getResponse(),
        residence: array[6].getResponse(),
        street: array[7].getResponse(),
        hnumber: array[8].getResponse(),
        mnumber_1: array[9].getResponse(),
        mnumber_2: array[10].getResponse(),
        email: array[11].getResponse(),
        grade: array[12].getResponse(),
    }

    var body = doc.getBody();
    // Header
    var header = body.appendParagraph("HEADER GOES HERE");
    header.setHeading(DocumentApp.ParagraphHeading.HEADING2).setAlignment(DocumentApp.HorizontalAlignment.CENTER);

    // Sub-Header
    var subheader = body.appendParagraph("SUB-HEADER GOES HERE");
    subheader.setHeading(DocumentApp.ParagraphHeading.HEADING4).setAlignment(DocumentApp.HorizontalAlignment.CENTER);

    // Body
    body.appendParagraph(
            "First name: " + person.fname +'\n'+
            "Last name: " + person.lname +'\n'+
            "Father name: " + person.Fname +'\n'+
            "Mother name: " + person.Mname +'\n'+
            "Father's job: " + person.Fjob +'\n'+
            "Mother's job: " + person.Mjob +'\n'+
            "Residence: " + person.residence +'\n'+
            "Street: " + person.street +'\n'+
            "Home phone number: " + person.hnumber +'\n'+
            "Mobile: " + person.mnumber_1 +'\n'+
            "2nd Mobile: " + person.mnumber_2 +'\n'+
            "Email: " + person.email +'\n'+
            "School Grade: " + person.grade +'\n'+

    // Footer
    var footer = body.appendParagraph("Legal Guardian");
    footer.setHeading(DocumentApp.ParagraphHeading.HEADING5).setAlignment(DocumentApp.HorizontalAlignment.RIGHT);
    doc.saveAndClose();
    return doc;
}

function createPDF(file){
    docblob = file.getAs('application/pdf');
    docblob.setName(file.getName() + ".pdf");
    ffile = DriveApp.createFile(docblob);
    DriveApp.getFoldersByName('FinalPDFs').next().addFile(ffile);
    return ffile
}

function sendMail(array, pdffile){
    var file1 = DriveApp.getFileById(pdffile.getId());
    MailApp.sendEmail(array[11].getResponse(),
            'SUBJECT OF EMAIL GOES HERE', 
            'EMAIL BODY GOES HERE. ANY NEWLINES CAN BE INSERTED USING \n', 
            {
                name: 'SENDERS NAME GOES HERE',
                attachments: [file1.getAs(MimeType.PDF)]
            }
            );
}

