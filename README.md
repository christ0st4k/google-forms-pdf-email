# Google Forms - PDF - email

![Google Apps Script Image](images/gas.png)

In this project you will find code that runs upon form submission event. It
creates a google document with text fields from the submitted form. Then it
creates a PDF and sends it back to the submitter.

## _Why I wrote this little project_

In a summer camp we wanted to make digital application forms for the parents to
be able to register their children via a website. We searched for the simplest
solution, and this was to use Google Forms. After this, we wanted to send a
copy of the form to the parents, to be able to sign it and bring it to us. And
here it is!

## Basic Documentation

In the section below you can find some basic explanation of functions that are
used in the code. The code is written in Google Apps Script language, which is
practically javascript with libraries and predefined variables and objects to
access Google related stuff (Forms, Drive documents, Email app etc). Below code
explanation you can find screenshots which guide you, how to enable such a
script.

_Official Documentation can be found [here](https://developers.google.com/apps-script/reference)_

### On form submission

When the submitter presses submit, he triggers the execution of the function
onFormSubmit()(This is set from the Triggers section from the gui of script
editor _See the last image in steps described in the end of this guide_). The
code takes the last submission, creates a Google Document in Google Drive,
makes a PDF from this file and finally emails it to the submitter.

```javascript
function onFormSubmit() {
    // You can refer to the form as the active one because this script is "connected" with it.
    var form = FormApp.getActiveForm();
    var all_form_responses = form.getResponses();

    // The line below takes all the responses of the last form as an array.
    var last_response_array = all_form_responses[all_form_responses.length-1].getItemResponses();
    sendMail(last_response_array,createPDF(docCreation(last_response_array)));
}
``` 
### Google Doc Creation

The function below creates a Google Document with specific name and fields. It
saves it into DocumentsFromResponses folder in Google Drive. It finally returns
the doc file to be used by an other function.

```javascript
function docCreation(array){  
    // Set the filename from the name and last name given in the form
    var filename = array[1].getResponse() + '_' + array[0].getResponse();  
    doc = DocumentApp.create(filename);  
    var docFile = DriveApp.getFileById(doc.getId());  
    DriveApp.getFoldersByName('DocumentsFromResponses').next().addFile(docFile); 

    // Variable to keep text fields from the Form
    var person = {
        fname: array[0].getResponse(), // first name
        lname: array[1].getResponse(), // last name
        Fname: array[2].getResponse(), // Father name
        Mname: array[3].getResponse(), // Mother name
        Fjob: array[4].getResponse(),
        Mjob: array[5].getResponse(),
        residence: array[6].getResponse(),
        street: array[7].getResponse(),
        hnumber: array[8].getResponse(),
        mnumber_1: array[9].getResponse(),
        mnumber_2: array[10].getResponse(),
        email: array[11].getResponse(),
        grade: array[12].getResponse(),
    }

    // "Write" info into the Document
    var body = doc.getBody();

    // Header
    var header = body.appendParagraph("HEADER GOES HERE");
    header.setHeading(DocumentApp.ParagraphHeading.HEADING2).setAlignment(DocumentApp.HorizontalAlignment.CENTER);

    // Sub-Header
    var subheader = body.appendParagraph("SUB-HEADER GOES HERE");
    subheader.setHeading(DocumentApp.ParagraphHeading.HEADING4).setAlignment(DocumentApp.HorizontalAlignment.CENTER);

    // Body
    body.appendParagraph(
            "First name: " + person.fname +'\n'+
            "Last name: " + person.lname +'\n'+
            "Father name: " + person.Fname +'\n'+
            "Mother name: " + person.Mname +'\n'+
            "Father's job: " + person.Fjob +'\n'+
            "Mother's job: " + person.Mjob +'\n'+
            "Residence: " + person.residence +'\n'+
            "Street: " + person.street +'\n'+
            "Home phone number: " + person.hnumber +'\n'+
            "Mobile: " + person.mnumber_1 +'\n'+
            "2nd Mobile: " + person.mnumber_2 +'\n'+
            "Email: " + person.email +'\n'+
            "School Grade: " + person.grade +'\n'+

    // Footer
    var footer = body.appendParagraph("Legal Guardian");
    footer.setHeading(DocumentApp.ParagraphHeading.HEADING5).setAlignment(DocumentApp.HorizontalAlignment.RIGHT);
    doc.saveAndClose();
    return doc;
}
```

_I could avoid the "hard-coded" lines of code by creating a dictionary with
names and ID's and iterate it with a loop, but I thought of it a little late so
I left it as it was._

### PDF Creation

Here the Google Document is converted in PDF. _ffile stands for final file_

```javascript
function createPDF(file){
    docblob = file.getAs('application/pdf');
    docblob.setName(file.getName() + ".pdf");
    ffile = DriveApp.createFile(docblob);
    DriveApp.getFoldersByName('FinalPDFs').next().addFile(ffile);
    return ffile
}
```

### Email the PDF

In this last "step" we send the above created PDF by e-mail. We set the
receiver's email by the correspondent form field

```javascript
function sendMail(array, pdffile){
    var file1 = DriveApp.getFileById(pdffile.getId());
    MailApp.sendEmail(array[11].getResponse(),
            'SUBJECT OF EMAIL GOES HERE', 
            'EMAIL BODY GOES HERE. ANY NEWLINES CAN BE INSERTED USING \n', 
            {
                name: 'SENDERS NAME GOES HERE',
                attachments: [file1.getAs(MimeType.PDF)]
            }
            );
}
```
## Enable the script

- In the Google Form you have created, go to `More` field and click on `Script
Editor`

![form-script-editor](images/form_click.png)

- You will be redirected into a new page with an "IDE" to write your code

![IDE](images/initial_editor.png)

 There you can paste the code from this repo and make any changes to bring it to your specidic needs. You can test the code by selecting a function and click run.

![Run](images/debug.png)

- Enable Trigger to run uppon Form Submission

1.

![trigger](images/trig_section.png)

2.

![add-trigger](images/add_trigger.png)

3.

![save-trigger](images/save_trigger.png)

**This code wants to use your Drive and the e-mail app. The first time that you
will try to run the code, you will need to give authorize your "application" to
control all those things.**

_Google allows you to run scripts but limits the amound of requests on specific
time. We didn't have any problems with our impementation because we had very
low "traffic". Otherwise you will need to pay an amount to Google to be able to
run it_

For any mistakes, comments or a proposal to make this better, you are more than welcomed to
open an issue! Hope it will save you some time!

